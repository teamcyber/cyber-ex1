import sys
from scapy.all import *
from scapy.layers.inet import IP, TCP

final_container = []
flag = ''

def scan(cur_packet):
    # ################################## IP ###################################
    if IP in cur_packet:
        if cur_packet.ttl < 100:
            opsys = 'Linux'
        else:
            opsys = 'Windows'
        do_print("IP (TTL), " + cur_packet[IP].src + ", " + opsys)

    # ################################## TCP ###################################

    if TCP in cur_packet:
        if cur_packet.options != 0:
            opsys = "Linux"
        else:
            opsys = "Windows"
        do_print("TCP (options), " + cur_packet[IP].src + ", " + opsys)

    # ################################## HTTP ###################################

    if cur_packet.haslayer(Raw):
        packet_str = str(cur_packet[Raw])
        final_string = ''
        if packet_str.startswith('GET'):
            reg = re.compile('User-Agent: ')
            match = reg.search(packet_str)
            trimmed = packet_str[match.end():]
            ua = trimmed.split('\r\n')[0]
            operating_system = ua.split(') ')[0]
            browser = ua.split(') ')[len(ua.split(') ')) - 1]

            if 'Linux' in operating_system:
                stri = ' Linux'
                if 'x86_64' in operating_system:
                    stri += ' x86_64'
                final_string = final_string + 'Ubuntu' + stri
            elif 'Windows' in operating_system:
                stri = ''
                if '10' in operating_system:
                    stri += ' 10'
                if '64' in operating_system:
                    stri += ' x64'
                final_string = final_string + 'Windows' + stri
            else:
                final_string += operating_system

            final_string += ','
            reg = re.compile('Chromium|Chrome|Firefox')
            match = reg.search(browser)
            if match > 0:
                res = browser[match.start():].split(' ')[0]
                final_string = final_string + ' ' + res
            else:
                final_string += browser

            do_print("HTTP (User-Agent), " + cur_packet[IP].src + ", " + final_string)

def do_print(final_str):
    if flag == '-s':
        print final_str
    else:
        final_container.append(final_str)

if __name__ == '__main__':
    if len(sys.argv) not in range(2, 4):
        exit('Invalid arguments!')
    flag = sys.argv[1]

    if flag == '-s':
        pkts = sniff(prn=scan)

    if flag == '-f':
        pkts = scapy.utils.rdpcap(sys.argv[2])
        for pkt in pkts:
            scan(pkt)
        for line in set(final_container):
            print line



        ###[ HTTP Request ]###
        # Method    = 'GET'
        # Path      = '/'
        # Http-Version= 'HTTP/1.1'
        # Host      = 'www.github.com'
        # User-Agent= 'Wget/1.13.4 (linux-gnu)'
        # Accept    = '*/*'
        # Accept-Language= None
        # Accept-Encoding= None
        # Accept-Charset= None
        # Referer   = None
        # Authorization= None
        # Expect    = None
        # From      = None
        # If-Match  = None
        # If-Modified-Since= None
        # If-None-Match= None
        # If-Range  = None
        # If-Unmodified-Since= None
        # Max-Forwards= None
        # Proxy-Authorization= None
        # Range     = None
        # TE        = None
        # Cache-Control= None
        # Connection= 'Keep-Alive'
        # Date      = None
        # Pragma    = None
        # Trailer   = None
        # Transfer-Encoding= None
        # Upgrade   = None
        # Via       = None
        # Warning   = None
        # Keep-Alive= None
        # Allow     = None
        # Content-Encoding= None
        # Content-Language= None
        # Content-Length= None
        # Content-Location= None
        # Content-MD5= None
        # Content-Range= None
        # Content-Type= None
        # Expires   = None
        # Last-Modified= None
        # Cookie    = None
        # Additional-Headers= None
