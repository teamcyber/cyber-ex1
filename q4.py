from scapy.all import *
from scapy.layers.inet import IP


def is_local(ip):
    # private address range:
    # 10.0.0.0 - 10.255.255.255
    # 172.16.0.0 - 172.31.255.255
    # 192.168.0.0 - 192.168.255.255

    split_ip = ip.split('.')
    first_octate = int(split_ip[0])
    second_octate = int(split_ip[1])

    if first_octate == 10:
        return True
    if first_octate == 172 and second_octate in range(16, 31):
        return True
    if first_octate == 192 and second_octate == 168:
        return True
    return False


def main():
    if len(sys.argv) != 3:
        exit('Usage: q4 [flag] [filename]\nflag = -f\nfilename = name of .pcap file')

    flag = sys.argv[1]
    file_name = sys.argv[2]

    try:
        addresses = []  # all ip addresses in the pcap, in the form of (src_ip, dst_ip, dst_mac).
        local_destinations = {}  # all private ip addresses in the pcap.
        default_gateways = []  # ip addresses of all found default gateways.

        # read the pcap file
        packets = scapy.utils.rdpcap(file_name)

        # get all address tuples from pcap file
        for current_packet in packets:
            if IP in current_packet and Ether in current_packet:
                src_ip = str(current_packet[IP].src)
                src_mac = str(current_packet[Ether].src)
                dst_ip = str(current_packet[IP].dst)
                dst_mac = str(current_packet[Ether].dst)

                # if any address is private, add to local destinations
                if is_local(dst_ip) and local_destinations.get(dst_mac) is None:
                    local_destinations[dst_mac] = dst_ip
                if is_local(src_ip) and local_destinations.get(src_mac) is None:
                    local_destinations[src_mac] = src_ip

                # add to addresses
                if (src_ip, dst_ip, dst_mac) not in addresses:
                    addresses.append((src_ip, dst_ip, dst_mac))

            # if arp response received, the mac address it came from is of the default gateway
            # since the request was first sent to it.
            if ARP in current_packet and Ether in current_packet and current_packet[ARP].op == 2:
                dgw_mac = current_packet[Ether].src
                dgw_ip = local_destinations.get(dgw_mac)

                # check this dgw was already printed
                if dgw_ip not in default_gateways:
                    default_gateways.append(dgw_ip)
                    print 'Default Gateway was found on ' + dgw_ip + ' (' + dgw_mac + ')'

        # after discovering all devices in the pcap we can find the default gateways
        for address_tuple in addresses:
            src_ip = address_tuple[0]
            dst_ip = address_tuple[1]
            dst_mac = address_tuple[2]

            # get the ip of the dgw from dst mac if it is local, else None will be returned
            default_gateway_ip = local_destinations.get(dst_mac)

            # if dgw ip was found and the src is local and the dst is remote, this ip is truly of a dgw
            if default_gateway_ip is not None and default_gateway_ip not in default_gateways and \
                    not is_local(dst_ip) and is_local(src_ip):
                default_gateways.append(default_gateway_ip)
                print 'Default Gateway was found on ' + default_gateway_ip + ' (' + dst_mac + ')'
    except IOError:
        exit('Invalid pcap file.')


if __name__ == '__main__':
    main()
