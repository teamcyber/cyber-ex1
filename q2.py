import socket
import sys


def construct_http_message(host, port, resource):
    msg = 'GET ' + resource + ' HTTP/1.0\r\n'
    msg += 'Host: ' + host + ':' + str(port) + '\r\n'
    msg += 'Connection: Keep-Alive\r\n'
    msg += 'Content-Type: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\n'
    msg += 'User-Agent: Opera/12.02 (Android 4.1; Linux; Opera xMobi/ADR-1111101157; U; en-US) ' \
           'Presto/2.9.201 Version/12.02\r\n'
    msg += 'Accept-Encoding: gzip, deflate\r\n'
    msg += 'Accept-Language: en-US,en;q=0.8,he;q=0.6,ru;q=0.4,ar;q=0.2\r\n'
    msg += '\r\n'

    return msg


def main():
    # check that 2 parameters were received
    if len(sys.argv) != 3:
        exit("Insufficient Arguments!")

    # get ip and port to send message to.
    server_ip = sys.argv[1]
    server_port = int(sys.argv[2])

    # check port validity. IP not checked since it can also be a DNS name.
    if server_port > 65356:
        exit("Invalid Port!")

    # open a new INET TCP socket.
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # connect to the server.
    sock.connect((server_ip, server_port))
    resource = '/'  # the resource to get from server.
    # construct a valid HTTP message.
    message = construct_http_message(server_ip, server_port, resource)

    # send the message.
    sent = 0
    while sent < len(message):
        sent = sock.send(message[sent:])
        sent += sent

    # OPTIONAL - print the server response
    # response = sock.recv(2048)
    # print 'Response:\n' + response


if __name__ == '__main__':
    main()
